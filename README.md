# go-e-pvsd

## Ein Daemon für PV-Überschussladen mit einem go-e-Charger

Das Programm ist ein [Python](https://www.python.org/)-Daemon, der unter der [GNU General Public License (GPL)](https://www.gnu.org/licenses/#GPL) veröffentlicht wird.

Der Daemon ermöglicht PV-Überschussladen mit einem [go-e-Charger](https://go-e.com/) (mittels der HTTP/JSON-API), und zwar mit minimalem Aufwand und einer übersichtlichen Codebasis – ohne von zusätzlicher Hardware wie dem go-e-Controller oder von externen Softwarepaketen bzw. Frameworks abzuhängen.

Folgende Python-Pakete werden benutzt:
 * [python-daemon](https://pypi.org/project/python-daemon/): Um den Prozess als Daemon laufen lassen zu können
 * [requests](https://pypi.org/project/requests/): Für die HTTP-Kommunikation mit dem Wechselrichter und der Ladestation
 * [pymodbus](https://pypi.org/project/pymodbus/): Optional, kann auch weggelassen werden. Vom Fronius-Backend benutzt, um die maximale Entladeleistung des Haus-Akkus anpassen zu können

Je nach Distribution sind die Pakete ganz oder teilweise sowieso vorhanden, oder können zumindest problemlos installiert werden.

Unter Gentoo gibt es z. B. `dev-python/python-daemon` und `dev-python/requests`. `pymodbus` ist in verschiedenen Overlays zu finden. Bei Artix gibt es `python-requests`. Für die beiden anderen kann man auf das AUR zurückgreifen. Devuan hat alles dabei, eine aktuelle Version von `pymodbus` ggf. in den Backports.

### Stand der Entwicklung

Ich lade mein E-Auto seit Herbst 2023 vorzugsweise mit PV-Überschuss, und dazu ist der vorliegende Daemon im Dauereinsatz.

Bisher gibt es aber noch kein Release, da ich immer noch am Optimieren des Daemons selbst und auch des Web-Interfaces bin. Es kann sich also noch unangekündigt viel ändern. Sobald ich eine längere Zeit überhaupt nichts mehr geänert habe (man also von „Feature Completeness“ sprechen kann), tagge ich Version 1.0.0.

## Was tut es?

Es geht darum, ein E-Auto mit möglichst viel eigenem Strom zu laden.

Hierfür stehen zwei Operationsmodi zur Verfügung: Das reine Überschussladen und Laden unter Zuhilfenahme von Netzstrom, aber mit möglichst großem PV-Anteil. Hierfür kann die Schwelle zum Laden dynamisch angepasst werden, und beim Laden ggf. die Entladung des Haus-Akkus deaktiviert werden (s. u.).

### Reines Überschussladen

Wenn ein Auto an die Ladestation angeschlossen wird, dann fängt der Daemon an, den PV-Überschuss zu überwachen und das Laden zu steuern. Wenn das Auto abgesteckt wird, geht er wieder in Wartestellung und liest keine Daten mehr vom Wechselrichter aus, sondern wartet nur noch darauf, dass wieder ein Auto angesteckt wird (wozu der Auto-Zustand periodisch von der Ladestation abgefragt wird).

Wenn der PV-Überschuss überwacht wird, dann liest das Programm periodisch die Leistungsdaten des Wechselrichters bzw. Smart-Meters sowie die des go-e-Chargers aus, und startet bzw. beendet das Laden, wenn genügend PV-Überschuss (oder eben nicht mehr genug) vorhanden ist. Weiterhin wird der Ladestrom kontinuierlich so angepasst, dass alle überschüssige Leistung im Auto-Akku und nicht im Netz landet, aber kein Strom zum Laden aus dem Netz bezogen wird. Auch eine Umschaltung zwischen ein- und dreiphasigem Laden wird unterstützt.

Unabhängig von der Ladestation kann ein Energielimit gesetzt werden, um z. B. nur bis zu einer definierten Prozentzahl zu laden. Außerdem überwacht der Daemon auch ein durch die Ladestation gesetztes Energielimit und beendet das Laden, sobald es erreicht wurde. Das tut er auch dann, wenn z. B. zwischenzeitlich das Auto kurz abgesteckt wurde und die Ladestation wieder von vorn mit dem Zählen anfangen würde.

Weiterhin kann man entscheiden, ob mit dem Beginn des Ladevorgangs so lange gewartet werden soll, bis ein evtl. vorhandener Haus-Akku geladen ist, oder ob direkt das Auto geladen werden soll, sofern genügend Leistung vorhanden ist.

### Teilweises Überschussladen

Reicht die Leistung der PV-Anlage typischerweise allein nicht mehr aus, kann man Netzstrom zum Laden nutzen, aber trotzdem allen PV-Strom nutzen. Hierfür kann die Schwelle für das einphasige Laden herabgesetzt werden. Wenn man z. B. die Schwelle von den voreingestellten 1400 W auf 700 W senkt, dann würde das Laden mit 6 A schon bei 700 W gestartet werden. Die fehlenden 700 W kommen dann aus dem Netz. Die Grenze kann ebenso auf 1 W gesetzt werden, dann wird nur geladen, wenn es überhaupt irgend einen Überschuss gibt – oder auf 0 W, dann wird immer geladen.

Egal, auf was die Schwelle steht – wenn es doch genügend Überschuss gibt, wird der Ladestrom so angepasst, dass aller Überschuss zum Laden benutzt wird.

Sofern der Wechselrichter das kann und einer vorhanden ist, kann das Entladen eines Haus-Akkus in diesem Modus automatisch verhindert werden, sobald das Laden des Autos startet. Damit wird dann sichergestellt, dass man nicht einen Akku mit einem anderen lädt; außerdem wäre ein typischer Haus-Akku ja hierfür sowieso viel zu klein. Sobald nicht mehr geladen wird, wird das Entladen des Haus-Akkus dann auch wieder erlaubt.

### Laufzeitinformationen und -steuerung

Die aktuellen Daten sowie die Konfiguration werden von einem minimalistischen HTTP-Server als JSON zur Verfügung gestellt. Weiterhin kann über die HTTP-Schnittstelle auch zur Laufzeit der Controller (der das eigentliche Überschussladen steuert) angesprochen werden.

Eine Beispiel-Web-Interface, das mit dem Server kommunizuert, ist unter `Interface/` zu finden. Das zeigt alle Funktionalitäten (und ich benutze es selbst zum Steuern und Monitoren des Daemons).

## Welche Ladestationen werden unterstützt?

Wie der Name schon sagt, ist das Programm für den go-e-Charger geschrieben. Andere Ladestationen werden nicht unterstützt.

Prinzipiell sollte es aber kein allzu großes Problem sein, die Kommunikation mit der Ladestation dahingehend zu abstrahieren, dass man auch andere Modelle ansprechen kann. Sollte jemand dieses Projekt mit einer anderen Ladestation als dem go-e-Charger betreiben: Gerne melden!

## Welche Wechselrichter werden unterstützt?

Momentan gibt es nur ein Backend: Eines für einen [SYMO-Gen24-Wechselrichter von Fronius](https://www.fronius.com/de/solarenergie/eigenheim/produkte-und-loesungen/photovoltaik/gen24-plus-wechselrichter-fuer-pv-anlage-notstrom) – das ist der, den ich habe.

Weitere Backends hinzuzufügen sollte aber ziemlich einfach sein:

Das Backend muss eine Datei sein, die in `InverterBackends/` liegt, und eine Klasse enthalten, die `Backend` heißt. Man kann diese von der `LoggingBase`-Klasse aus `shared/LoggingBase.py` ableiten, diese Basisklasse stellt die Logging-Funktionalität als `self.log()` zur Verfügung.

Weiterhin muss die Klasse eine Funktion namens `getPowerData()` haben, die als Rückgabewert den Erfolg der Abfrage (`bool`) sowie die momentane Netzeinspeisung (negativer Wert) bzw. den Bezug (positiver Wert) und die momentane Haus-Akku-Nutzung (negativ: Laden, positiv: Bezug) als `float` zurückgibt (oder entsprechend immer `0.0` für den Akku, wenn keiner da ist).

Außerdem muss die Klasse noch die Funktion `disableBattery(state: bool)` bereitstellen, mit der die Entladung eines Haus-Akkus verhindert bzw. erlaubt wird. Sofern das nicht gewünscht oder möglich ist, kann die Funktion einfach immer `True` zurückgeben.

Ein Blick in `InverterBackends/FroniusGen24.py` sollte eigentlich alles hinreichend erklären, das Backend ist ziemlich übersichtlich.

## Benutzung

Das Programm nutzt nur minimale Systemressourcen. Es muss einfach nur auf einem Rechner ausgeführt werden, der im selben Netzwerk ist, wie der go-e-Charger und der Wechselrichter (resp. das auszulesende Smart-Meter). Beispielsweise kann hierfür ein Einplatinencomputer wie ein [Raspberry Pi](https://raspberrypi.com/) (Version 1 reicht sicherlich), ein ohnehin laufender NAS-Rechner o. Ä. zum Einsatz kommen, oder aber auch ein beliebiger Desktop-Computer.

### Installation

Momentan ist go-e-pvsd kein „richtiges“ Python-Paket. Das steht noch aus. Der Einfachheit halber sollte man derzeit den Code einfach irgendwohin entpacken, und `go-e-pvsd` nach `/usr/bin/` verlinken, ggf. auch das Init-Script nach `/etc/init.d/`.

### Konfiguration

Die Konfiguration erfolgt über ini-Dateien. Zunächst wird nach `/etc/go-e-pvsd.ini` gesucht, dann nach `~/.config/go-e-pvsd.ini`, und schließlich nach `go-e-pvsd.ini` im aktuellen Verzeichnis. Einstellungen von später gefundenen Dateien überschrieben die von früher gefundenen.

Werden Einstellungen nicht gesetzt, werden die Standardwerte benutzt, die in den jeweiligen Klassen definiert sind.

Die jeweiligen Abschnitte der Konfiguration (Daemon-Starter, Wechselrichter-Backend und Controller) werden alle in derselben Datei konfiguriert, jeweils als Abschnitt.

Vorsicht: Bei allen Schlüsseln und Abschnitten wird zwischen Groß und Kleinschreibung unterschieden!

#### Daemon-Starter

Die Konfiguration des Starters erfolgt im Abschnitt `[main]`. Folgende Einstellungen können gesetzt werden:

`serverBindAddress`
: Die IP-Adresse, an die der HTTP-Server bindet. Standard ist `127.0.0.1`, damit ist der Server nur auf dem lokalen Rechner erreichbar. Um ihn im ganzen Netzwerk zur Verfügung zu stellen, diese Einstellung auf leer setzen, also in der der ini-Datei einfach `serverBindAddress =`. Damit bindet der Server an alle Interfaces. Resp. die IP-Adresse des jeweiligen Rechners angeben.

`serverPort`
: Der Port, auf dem der HTTP-Server erreichbar sein soll. Standard ist `8008`.

`inverterBackend`
: Das zu benutzende Wechselrichter-Backend. Standard ist `FroniusGen24`.

`heartbeat`
: Die Anzahl Sekunden, nach denen alle Werte neu abgefragt werden. Vorsteinstellung ist `3`.

#### FroniusGen24-Backend

Die Konfiguration erfolgt in `[FroniusGen24]`. Folgende Werte können gesetzt werden:

`inverterIp`
: Die IP-Adresse des Wechselrichters. Die Voreinstellung ist `192.168.1.100`, muss aber höchstwahrscheinlich individualisiert werden.

`requestTimeout`
: Der Timeout in Sekunden für eine HTTP-Anfrage (Voreinstellung: `10`)

`batteryPresent`
: Zeigt an, ob ein Haus-Akku vorhanden ist und abgefragt werden soll. Voreinstellung ist `True`.

`modusTcpPort`
: Der Port, auf dem der Modbus-TCP-Server des Wechselrichters erreichbar ist. Wird nur benutzt, wenn die Entladeleistung des Haus-Akkus angepasst werden soll (sofern `disableBattery` in der `Controller`-Section aktiviert ist). Normalerweise ist der Standardwert `502` passend.

#### Controller

Die Konfiguration des Controllers (das ist der Teil von go-e-pvsd, der die eigentliche Steuerung übernimmt) passiert in `[Controller]`. Folgende Werte können gesetzt werden:

`chargerIp`
: Die IP-Adresse des go-e-Chargers. Voreinstellung ist `192.168.1.100`, aber dieser Wert muss aller Wahrscheinlichkeit nach angepasst werden.

`requestTimeout`
: Der Timeout in Sekunden für eine HTTP-Anfrage (Standardwert: `10`)

`averaging`
: Anzahl von Messwerten, die gemittelt werden sollen (Standard: `5`)

`onePhaseMinP`
: Minimale Überschussleistung in Watt für einphasiges Laden (Standard: `1400`)

`threePhasesMinP`
: Minimale Überschussleistung in Watt  für dreiphasiges Laden (Standard: `4200`)

`priorizeBattery`
: Laden des Haus-Akkus priorisieren oder nicht. Standard ist `False`. In diesem Fall wird der Ladestrom des Akkus zum Überschuss addiert. Sobald genügend Überschuss vorhanden ist, wird das Auto geladen (der Haus-Akku bekommt dann nur noch das, was übrig bleibt). Wenn das Laden des Haus-Akkus priorisiert ist, dann wird der Ladestrom nicht als Überschuss betrachtet. Für das Laden des Autos bleibt also nur der Rest (und der ist normalerweise nichts, solang der Akku nicht vollständig geladen ist). Das Priorisieren des Haus-Akkus kann über die HTTP-Schnittstelle zur Laufzeit ein- und ausgeschaltet werden (s. u.).

`disableBattery`
: Ist dieser Wert `True`, dann wird das Entladen des Haus-Akkus deaktiviert, sobald man netzassistiert lädt (also mit einer geringeren Ladeschwelle für einphasiges Laden als der eigentlich konfigurierten). Das verhindert, dass ein Akku mit einem anderen geladen wird. Sobald nicht mehr geladen wird, wird das Entladen wieder erlaubt. Für das FroniusGen24-Backend muss hierfür der Modbus-TCP-Server am Wechselrichter aktiviert werden (vgl. z. B. [Akku an einem Fronius-Wechselrichter per Modbus steuern](https://nasauber.de/blog/2024/akku-an-einem-fronius-wechselrichter-per-modbus-steuern/)).

`useSurplusFraction`
: Der prozentuale Anteil (der Einfachheit halber nicht als Prozent, sondern als float, sinnvollerweise größer als 0 und kleiner oder gleich 1) des Überschusses, der zur Berechnung des Ladestroms benutzt wird. Je kleiner dieser Wert ist, desto mehr ist sichergestellt, dass kein Netz- oder Hausakkustrom zum Laden benutzt, sondern eher ein bisschen eingespeist wird (Voreinstellung: `0.97`).

`checkCarConnectedInterval`
: Das Intervall in Sekunden, nach dem überprüft wird, ob ein Auto angeschlossen ist. Die PV-Überwachung wird entsprechend gestartet bzw. beendet (Voreinstellung: `60`)

`ampereChangeHoldTime`
: Minimale Zeit in Sekunden, bevor eine Änderung des Ladestroms gemacht wird (Voreinstellung: `20`)

`onePhaseMinChargeTime`
: Minimale Zeit in Sekunden, bevor einphasiges Laden gestartet oder beendet wird (Voreinstellung: `60`)

`onePhaseAmpHoldTime`
: Minimale Zeit in Sekunden, bevor bei einphasigem Laden erstmals der Ladestrom neu berechnet wird (Voreinstellung: `30`)

`threePhasesMinChargeTime`
: Minimale Zeit in Sekunden, bevor dreiphasiges Laden gestartet oder beendet wird (Voreinstellung: `60`)

`threePhasesAmpHoldTime`
: Minimale Zeit in Sekunden, bevor bei dreiphasigem Laden erstmals der Ladestrom neu berechnet wird (Voreinstellung: `40`)

`chargingPauseHoldTime`
: Minimale Zeit in Sekunden, bevor Laden – nachdem es beendet wurde – erneut gestartet wird (Voreinstellung: `60`)

#### Beispiel

Eine Konfigurationsdatei könnte beispielsweise so aussehen:

    [main]
    serverBindAddress =

    [FroniusGen24]
    inverterIp = 192.168.178.4

    [Controller]
    chargerIp = 192.168.178.87
    priorizeBattery = True

### Kommandozeilenoptionen

Folgende Optionen können für das Start-Script gesetzt werden:

`-v`
: Es werden mehr Informationen ausgegeben

`-d`
: go-e-pvsd wird als Daemon (Systemdienst) im Hintergrund ausgeführt (s. u.)

`-p`
: Name der Pidfile für den Daemon-Modus

### Nutzung im Vordergrund

Zu Testzwecken bietet sich an, das Programm zunächst als Vordergrundprozess auszuführen. Das geht, indem man einfach `go-e-pvsd` aus dem Basisverzeichnis in einer Konsole startet (ohne die `-d`-Option).

Wenn der Daemon im Vordergrund läuft, dann wird nach `stdout` geloggt, also einfach auf die Konsole geschrieben.

Über `SIGINT` (`Strg+C`) kann das Programm beendet werden.

Für das Laufen im Vordergrund sind keine root-Rechte erforderlich.

### Nutzung als Systemdienst

Wenn go-e-pvsd mit der Kommandozeilenoption `-d` gestartet wird, dann wird ein Pidfile erstellt (standardmäßig `/run/go-e-pvsd.pid`, einstellbar über die Option `-p`), das Programm koppelt sich von der Shell ab und läuft dann im Hintergrund (als Daemon). Empfängt der Daemon `SIGTERM` (entweder über ein Init-System oder über `kill -s SIGTERM <pid>`), beendet er sich.

Für das Ausführen als Systemdienst sind zum Starten i. d. R. root-Rechte erforderlich.

Ein Init-Script für [OpenRC](https://wiki.gentoo.org/wiki/OpenRC) sowie ein klassisches Sysvinit-Init-Script sind in `init/` zu finden.

Das OpenRC-Script habe ich auf Gentoo und Artix/OpenRC erfolgreich getestet. Auf Devuan/OpenRC hat es nicht funktioniert (es konnte zwar den Daemon starten, aber nicht mehr beenden; wie ich jetzt weiß, liegt das daran, dass Devuan nicht `start-stop-daemon` von OpenRC nutzt, sondern die Debian-Variante davon, und die sind nicht 100 % kompatibel). Deswegen habe ich zunächst das Sysvinit-Init-Script geschrieben. Eine angepasste Version des OpenRC-Init-Scripts hat dann letztlich auch auf Devuan funktioniert, und liegt jetzt auch bei.

Sicher ist es auch nicht schwer, eine Systemd-Unit zu schreiben, ich nutze selbst aber Systemd nicht. Wenn einer das gemacht hat: Her damit!

### HTTP-Interface

Der Daemon startet einen minimalistischen HTTP-Server zur Kommunikation und Steuerung zur Laufzeit. Die Antwort des Servers ist immer UTF-8-kodierter reiner Text (je nach Anfrage entweder mit dem MIME-Typ `text/plain` oder `application/json`). Folgende Interaktion ist durch das Aufrufen einer URL, z. B. `http://127.0.0.1:8008/<Befehl>` möglich:

#### `config`

Alle beim Starten gesetzten Daemon-Einstellungen werden angefragt. Die Antwort wird als JSON-Objekt formatiert.

#### `status`

Alle momentanen Daten werden angefragt. Die Antwort wird als JSON-Objekt formatiert.

#### `restart`

Ein Neustart des Controllers wird angefragt. Dabei werden der momentane Wert des Energiezählers sowie ein ggf. gesetztes Energielimit neu ausgelesen, und die internen Ladezeit- und Energiezähler werden zurückgesetzt.

#### `limit?<Wh>`

Ein Energielimit für das Laden setzen. `Wh` sind die zu ladenden Wh als Float- oder Integerwert. Ein Limit kann dann gesetzt werden, wenn die Ladestation selbst kein Limit definiert. Ansonsten wird dieses Limit benutzt und nicht verändert.

Das Setzen eines Limits setzt ein altes Limit außer Kraft (durch das Setzen eines neuen Limits wird z. B. das Laden wieder gestartet, wenn vorher das alte Energielimit erreicht war).

#### `minimum?<restore|W>`

Den minimalen Überschuss für einphasiges Laden setzen oder wiederherstellen. Es wird der angegebene Wert als Minimum gesetzt. Wenn `restore` angegeben wird, dann wird der in der Konfigurationsdatei definierte Wert wiederhergestellt.

Das ist dafür gedacht, wenn in der Übergangszeit die eigentlich nötige minimale Leistung nicht mehr erreicht oder immer wieder unterschritten wird, aber auf jeden Fall geladen werden soll. Die fehlende Leistung für den minimalen Ladestrom wird aus dem Netz ergänzt (bzw. mit Strom aus dem Hausakku, sofern man das Entladen nicht deaktiviert hat).

#### `battery?<parameter>`

Setzen der Haus-Akku-Priorisierung, abhängig vom Wert von `parameter`:

##### `priorize`

Das Laden des Haus-Akkus dem Laden des Autos vorziehen. Die zum Laden des Haus-Akkus genutzte Leistung wird nicht dem Überschuss hinzugerechnet.

##### `unpriorize`

Das Laden des Autos dem Laden des Haus-Akkus vorziehen. Evtl. zum Laden des Haus-Akkus genutzte Leistung wird als Überschuss betrachtet und potenziell zum Laden des Autos genutzt.

### Beispiel-Web-Interface

Das Verzeichnis `Interface/` enthält ein Beispiel-Web-Interface, was das HTTP-Interface von go-e-pvsd nutzt. Das Interface benutzt ein kleines PHP-Script als Schnittstelle zum go-e-pvsd-Server, damit z. B. die aktuellen Daten via AJAX ausgelesen werden können ohne ein Problem mit der Same-Source-Policy zu bekommen. Weiterhin muss so der Server nicht zum Internet hin exponiert werden. Per Voreinstellung holt sich das Interface alle drei Sekunden via [jQuery](https://jquery.com/)-AJAX-Aufruf die aktuellen Daten und zeigt sie an.

Nachdem man eine `settings.js`-Datei erstellt hat (vgl. `settings.js.template`), kann man das Interface nutzen, sofern der go-e-pvsd-Server unter `http://127.0.0.1:8008/` erreichbar ist (das ggf. in `query_server.php` anpassen). Zum Ausprobieren kann man z. B. im Verzeichnis `Interface/` den PHP Development Server starten (für Port 8000 mittels `php -S 127.0.0.1:8000 -f .`). Dann kann man sich die Seite unter `http://127.0.0.1:8000/` anschauen.

Sinnvollerweise lässt man sein Interface später einen Rechner ausliefern, auf dem sowieso ein HTTP-Server läuft. Bitte zusätzlich absichern, z. B. mit HTTP-Auth (oder gleich gar nicht vom Internet aus verfügbar machen), damit kein Unfug damit gemacht wird!
