# SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from shared.LoggingBase import LoggingBase
from shared.JsonHelper import jsonQuery

from pymodbus.client import ModbusTcpClient

"""
To prevent drawing engergy from the house battery when charging, we use the inverter's Modbus TCP
server. Using the SunSpec int+SF model, we need two registers to turn the battery on or off:

Register 40349: StorCtl_Mod
    Possible values: Set charging limit   : 1 (0x01)
                     Set discharging limit: 2 (0x10)
                     Set both limits      : 3 (0x11)

Register 40356: OutWRte
    Possible values: 0-10000
    This value has a scale factor of -2, so we have to multply the values with 10^-2. Consequently,
    0 represents 0 % (no discharging allowed) and 10000 represents 100 % (discharging at full power
    is allowed).

The real used address is the register address minus 1, so we write to 40348 and 40355 respectively.
"""

class Backend(LoggingBase):
    """ Backend for communication with a Fronius SYMO Gen24 inverter """

    def __init__(self, logger, configHelper):
        # Pass the logger to the base class
        super().__init__(logger)

        # The following error codes can be returned by the inverter when talking to the inverter's
        # Modbus TCP server, according to the Fronius docs:
        self.modbusExceptionCodes = {
            1 : "ILLEGAL FUNCTION",
            2 : "ILLEGAL DATA ADDRESS",
            3 : "ILLEGAL DATA VALUE",
            4 : "SLAVE DEVICE FAILURE",
            11: "GATEWAY TARGET DEVICE FAILED TO RESPOND"
        }

        # Default settings

        self.config = {
            # The inverter's IP address
            "inverterIp"    : "192.168.1.100",
            # HTTP request timeout (seconds to wait)
            "requestTimeout": 10,
            # Query battery data
            "batteryPresent": True,
            # Modbus TCP port
            "modusTcpPort"  : 502
        }

        self.configTypes = {
            "inverterIp"    : str,
            "requestTimeout": int,
            "batteryPresent": bool,
            "modusTcpPort"  : int
        }

        # Update the settings with the found config
        self.config = configHelper.updateConfig(self.config, self.configTypes, "FroniusGen24")

    def getPowerData(self) -> (bool, float, float):
        grid = 0.0
        battery = 0.0

        # Get the reported grid feed-in or draw

        answer, error = jsonQuery("http://{}/solar_api/v1/GetMeterRealtimeData.cgi".format(
                                      self.config["inverterIp"]),
                                  { "Scope": "System" }, self.config["requestTimeout"])
        if error:
            self.log(error)
            return False, 0.0, 0.0

        try:
            grid = float(answer["Body"]["Data"]["0"]["PowerReal_P_Sum"])
        except:
            self.log("Could not extract the current grid feed-in/draw from the inverter's answer!")
            return False, 0.0, 0.0

        # If we don't have a battery, we return the grid feed-in or draw
        if not self.config["batteryPresent"]:
            return True, grid, 0.0

        # Otherwise check if the battery is discharging

        answer, error = jsonQuery("http://{}/solar_api/v1/GetStorageRealtimeData.cgi".format(
                                      self.config["inverterIp"]),
                                  { "Scope": "System" }, self.config["requestTimeout"])
        if error:
            self.log(error)
            return False, 0.0, 0.0

        try:
            i = float(answer["Body"]["Data"]["0"]["Controller"]["Current_DC"])
            u = float(answer["Body"]["Data"]["0"]["Controller"]["Voltage_DC"])
            battery = u * i * -1
        except:
            self.log("Could not extract the current battery feed-in/draw from the inverter's "
                     "answer!")
            return False, 0.0, 0.0

        # Return all values
        return True, grid, battery

    def checkModbusResponse(self, response) -> bool:
        """ Helper function to check a Modbus response and log errors """

        if not response.isError():
            return True

        error = "Unknown exception"

        if type(response).__name__ == "ModbusIOException":
            error = response.message

        elif type(response).__name__ == "ExceptionResponse" \
            and response.exception_code in self.modbusExceptionCodes:
            error = "{} ({})".format(self.modbusExceptionCodes[response.exception_code],
                                     response.exception_code)

        self.log("Modbus register write failed: {}".format(error))
        return False

    def disableBattery(self, state: bool) -> bool:
        """ Disable drawing energy from the battery or re-enable it again """

        client = ModbusTcpClient(host = self.config["inverterIp"],
                                 port = self.config["modusTcpPort"])

        if not client.connect():
            self.log("Could not connect to the Modbus TCP server!")
            return False

        self.log("    Setting StorCtl_Mod (register 40349) to 2", 1)
        response = client.write_register(40348, 2, slave = 1)
        if not self.checkModbusResponse(response):
            self.log("Failed to set StorCtl_Mod!")
            client.close()
            return False

        limit = 0 if state else 10000
        self.log("    Setting OutWRte (register 40356) to {}".format(limit), 1)
        response = client.write_register(40355, limit, slave = 1)
        if not self.checkModbusResponse(response):
            self.log("Failed to set OutWRte!")
            client.close()
            return False

        client.close()
        return True
