<?php

// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: AGPL-3.0-or-later

// This is a helper wrapper script that queries the go-e-pvsd server and passes its output through.
// This way, we can request the data via AJAX without getting problems with cross-resource requests
// and without having to expose the server to the internet.

// This silences the warning the following file_get_contents() call yields
// if the daemon isn't running and thus the URL is not accessible
set_error_handler(function() { /* do nothing */ });

$query = "";
if (isset($_GET["query"])) {
    $query = urldecode($_GET["query"]);
}

$response = file_get_contents("http://127.0.0.1:8008/$query");

if ($response == "") {
    header("HTTP/1.1 503 Service Unavailable");
    header("Content-Type: text/plain; charset=UTF-8");
    echo "Could not query the go-e-pvsd server";
    exit;
}

if ($query == "config" || $query == "status") {
    header("Content-Type: application/json; charset=UTF-8");
} else {
    header("Content-Type: text/plain; charset=UTF-8");
}

echo $response;
