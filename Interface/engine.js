// SPDX-FileCopyrightText: 2024 Tobias Leupold <tl at stonemx dot de>
//
// SPDX-License-Identifier: BSD-2-Clause

// Global variables

let dameonConfig = {};
let meterLow = 0;
let meterOnePhase = 0;
let meterThreePhases = 0;

let currentStatus = {};
let isVisible = true;

let updateTimeoutId = null;
let responseTimeoutId = null;

// Start as soon as the document is completely loaded

document.addEventListener("DOMContentLoaded", function()
{
    if (   typeof(refresh)        === "undefined"
        || typeof(maxPv)          === "undefined"
        || typeof(capacity)       === "undefined"
        || typeof(kWhConsumption) === "undefined") {

        setTimeout(function()
        {
            requestFailed("Startup not possible: Please setup settings.js!");
        }, 0);
        return;
    }

    // Try to restore the last entered limit values
    const limitPercentActual = localStorage.getItem("limitPercentActual");
    if (limitPercentActual !== null) {
        $("#percent_actual").val(limitPercentActual);
    }
    const limitPercentNominal = localStorage.getItem("limitPercentNominal");
    if (limitPercentNominal !== null) {
        $("#percent_nominal").val(limitPercentNominal);
    }

    // Restore the last entered minimum override value
    const minimumOnePhaseP = localStorage.getItem("minimumOnePhaseP");
    if (minimumOnePhaseP !== null) {
        $("#one_phase_p").val(minimumOnePhaseP);
    }

    // Cache the daemon's config

    $.getJSON(requestUrl("config"))

    .fail(function(request, textStatus, errorThrown)
    {
        requestFailed(request.responseText);
    })

    .done(function(json)
    {
        daemonConfig = json;
        meterLow = round(daemonConfig["onePhaseMinP"] / maxPv * 100.0);
        meterOnePhase = round((daemonConfig["threePhasesMinP"] - daemonConfig["onePhaseMinP"])
                              / maxPv * 100.0);
        meterThreePhases = 100 - meterOnePhase - meterLow;
        update();
    });
});

// Track if the current tab/window goes out of view or is shown again

document.addEventListener("visibilitychange", function()
{
    // Don't update if we're not visible and continue if we're visible again
    isVisible = ! document.hidden;
    if (isVisible) {
        update();
    }
});

// Assemble a request URL

function requestUrl(query)
{
    return "query_server.php?query=" + encodeURIComponent(query);
}

// Display a failed request, removing all other content from the page

function requestFailed(text)
{
    $("#status").css("display", "none");
    $("#request_failed").css("display", "inline");
    $("#request_error").html(text);
}

// Convert a go-e-pvsd server date to a JavaScript date

function parseDate(date)
{
    return new Date(Date.parse(date));
}

// Format a date, either a go-e-pvsd server one or a JavaScript one

function formatDate(date)
{
    let dateObject = null;
    if (typeof(date) == "string") {
        // We're working on a time string from the server, so we have to parse it first
        dateObject = parseDate(date);
    } else {
        // We work on a JS date object
        dateObject = date;
    }

    // Yeah, Date.getMonth() actually returns 0-11, and the function for getting the day of
    // a date is actually called Date.getDate(). What the hell did they smoke back then?!
    return         String(dateObject.getFullYear())
           + "-" + String(dateObject.getMonth() + 1).padStart(2, "0")
           + "-" + String(dateObject.getDate()).padStart(2, "0")
           + " "
           +       String(dateObject.getHours()).padStart(2, "0")
           + ":" + String(dateObject.getMinutes()).padStart(2, "0")
           + ":" + String(dateObject.getSeconds()).padStart(2, "0");
}

// Calculate the start date x seconds ago

function getStartDateTime(ago)
{
    const timestamp = Math.floor(Date.now() / 1000.0)                     // Now
    const lastUpdate = parseDate(currentStatus["lastUpdate"]);            // Last update
    const offset = timestamp - Math.floor(lastUpdate.getTime() / 1000.0); // Offset between

    const start = new Date((timestamp - offset - ago) * 1000);
    return formatDate(start);
}

// Parse seconds to hours, minutes and seconds

function formatSeconds(seconds)
{
    const s = seconds % 60;
    const m = Math.floor(seconds / 60) % 60;
    const h = Math.floor(seconds / 3600) % 24;
    const d = Math.floor(seconds / 3600 / 24);

    let ret = [];
    if (d > 0) {
        ret.push(d + " d");
    }
    if (h > 0) {
        ret.push(h + " h");
    }
    if (m > 0) {
        ret.push(m + " m");
    }
    if (s > 0) {
        ret.push(s + " s");
    }

    if (ret.length === 0) {
        return "0 s";
    }

    return ret.join(", ");
}

// Round to a given number of decimals

function round(value, decimals = 0)
{
    if (decimals == 0) {
        return Math.round(value);
    } else {
        return Number.parseFloat(value).toFixed(decimals);
    }
}

// Format Watt

function formatWatt(watt, sign)
{
    if (watt === null) {
        return "n/a";
    }

    let rounded = round(watt);
    if (sign && rounded > 0) {
        return "+" + rounded  + " W";
    } else if (sign && rounded < 0) {
        return rounded + " W";
    } else {
        return rounded + " W";
    }
}

// Helper function to assemble the "charging" text

function appendChargingText(chargingText, text)
{
    if (chargingText == "") {
        return text;
    } else {
        return chargingText + "<br>" + text;
    }
}

// Query the go-e-pvsd server for current values

function update()
{
    if (! isVisible) {
        // If we're not visible: Skip the update and don't schedule the next one.
        // Once we're visible again, isVisible will be set to true and update() will be called.
        return;
    }

    $.getJSON(requestUrl("status"))

    .fail(function(request, textStatus, errorThrown)
    {
        requestFailed(request.responseText);
    })

    .done(function(json)
    {
        currentStatus = json;
        display();
        updateTimeoutId = setTimeout(update, refresh);
    });
}

// Parse, format and display the current values

function display()
{
    // Request time and controller state info

    $("#answer_time").html(formatDate(currentStatus["timestamp"]));

    $("#last_update").html(formatDate(currentStatus["lastUpdate"]));

    $("#limit_queried").html(formatDate(currentStatus["limitRefreshed"]));

    if (currentStatus["started"]) {
        $("#controller_state").html("Started");
    } else {
        $("#controller_state").html("Stopped: " + currentStatus["stopReason"]);
    }

    // Charging info

    let chargingText = "";

    // Charging stopped section

    if (   ! currentStatus["chargingStartedOnePhase"]
        && ! currentStatus["chargingStartedThreePhases"]) {

        chargingText = "Charging stopped <br>"
                       + "(since " + getStartDateTime(currentStatus["chargingStopped"]) + "; "
                       + formatSeconds(currentStatus["chargingStopped"]) + ")";

    // Charging with x phases and y ampere section

    } else {
        const chargeStart = currentStatus["chargingStartedThreePhases"]
                            ? currentStatus["chargingStartedThreePhases"]
                            : currentStatus["chargingStartedOnePhase"];

        chargingText = "Charging with "
                       + (currentStatus["chargingStartedThreePhases"] ? "3 phases" : "1 phase")
                       + " at " + currentStatus["currentAmpere"] + " A" + "<br>"
                       + "(started " + getStartDateTime(chargeStart) + "; "
                       + formatSeconds(chargeStart) + ")";
    }

    // Limit undercut section

    if (currentStatus["chargingStartedThreePhases"]
        && currentStatus["minimumUndercutThreePhases"]) {

        const secs = daemonConfig["threePhasesMinChargeTime"]
                     - currentStatus["minimumUndercutThreePhases"];

        if (currentStatus["minimumReachedOnePhase"]) {
            if (secs > 0) {
                chargingText = appendChargingText(chargingText,
                    "Will switch to one-phase charging in " + secs + " s");
            } else {
                chargingText = appendChargingText(chargingText,
                    "Now switching to one-phase charging");
            }
        } else {
            if (secs > 0) {
                chargingText = appendChargingText(chargingText,
                                                  "Will stop charging in " + secs + " s");
            } else {
                chargingText = appendChargingText(chargingText,
                                                  "Now stopping charging");
            }
        }

    } else if (currentStatus["chargingStartedOnePhase"]
               && currentStatus["minimumUndercutOnePhase"]) {

        const secs = daemonConfig["onePhaseMinChargeTime"]
                     - currentStatus["minimumUndercutOnePhase"];

        if (secs > 0) {
            chargingText = appendChargingText(chargingText,
                                              "Will stop charging in " + secs + " s");
        } else {
            chargingText = appendChargingText(chargingText,
                                              "Now stopping charging");
        }
    }

    // Starting charging section

    if (currentStatus["chargingStopped"]
        && (currentStatus["minimumReachedOnePhase"] || currentStatus["minimumReachedThreePhases"])
        && (currentStatus["chargingStopped"] < daemonConfig["chargingPauseHoldTime"])) {

        const toGo = daemonConfig["chargingPauseHoldTime"] - currentStatus["chargingStopped"];
        chargingText = appendChargingText(chargingText,
            "Waiting for the minimum charging pause to pass (" + toGo + " s to go)");
    }

    if (! currentStatus["chargingStartedThreePhases"]
        && ! currentStatus["chargingStartedOnePhase"]
        && currentStatus["minimumReachedOnePhase"]
        && ! currentStatus["minimumReachedThreePhases"]) {

        const toGo = daemonConfig["onePhaseMinChargeTime"]
                     - currentStatus["minimumReachedOnePhase"];
        if (toGo > 0) {
            chargingText = appendChargingText(chargingText,
                "Will start one-phase charging in " + toGo + " s");
        } else {
            chargingText = appendChargingText(chargingText,
                "Now starting one-phase charging");
        }

    } else if (! currentStatus["chargingStartedThreePhases"]
               && currentStatus["minimumReachedThreePhases"]) {

        const toGo = daemonConfig["threePhasesMinChargeTime"]
                     - currentStatus["minimumReachedThreePhases"];

        if (toGo > 0) {
            chargingText = appendChargingText(chargingText,
                "Will start three-phase charging in " + toGo + " s");
        } else {
            chargingText = appendChargingText(chargingText, "Now starting three-phase charging");
        }
    }

    // Update the displayed text
    $("#charging_status").html(chargingText);

    // Current charging data

    let timeChargedText = formatSeconds(currentStatus["timeCharged"]);
    if (currentStatus["timeCharged"] > 0) {
        timeChargedText += "; " + round(currentStatus["chargedEnergy"] / 1000.0
                                        / currentStatus["timeCharged"] * 3600.0, 1) + " kWh/h";
    }
    $("#time_charged").html(timeChargedText);

    let chargedEnergy = round(currentStatus["chargedEnergy"] / 1000.0, 1) + " kWh; "
                        + round(currentStatus["chargedEnergy"] / capacity * 100.0, 1) + "&nbsp;%; "
                        + "~" + round(currentStatus["chargedEnergy"] / 1000.0 / kWhConsumption
                                      * 100.0)
                        + "&nbsp;km";
    $("#charged_energy").html(chargedEnergy);

    if (currentStatus["energyLimit"]) {
        let energyLimit = round(currentStatus["energyLimit"] / 1000.0, 1) + " kWh";
        if (currentStatus["chargedEnergy"] != 0) {
        energyLimit += " (" + round(currentStatus["chargedEnergy"] / currentStatus["energyLimit"]
                                    * 100.0, 1) + " % charged)";
        }
        $("#energy_limit").html(energyLimit);
    } else {
        $("#energy_limit").html("No limit set");
    }

    // Power info section
    $("#grid_average").html(formatWatt(currentStatus["gridAverage"], true));
    $("#grid").html(formatWatt(currentStatus["grid"], true));
    $("#battery_average").html(formatWatt(currentStatus["batteryAverage"], true));
    $("#battery").html(formatWatt(currentStatus["battery"], true));
    $("#charger_average").html(formatWatt(currentStatus["chargerAverage"], false));
    $("#charger").html(formatWatt(currentStatus["charger"], false));

    // Surplus section

    let surplusText = "Current surplus: " + formatWatt(currentStatus["surplus"], false);

    let surplusPercent = 0.0;
    if (currentStatus["chargerAverage"] > 0 && currentStatus["surplus"] > 0) {
        surplusPercent = round(currentStatus["chargerAverage"] / currentStatus["surplus"] * 100.0);
    }

    if (currentStatus["chargerAverage"] > 0) {
        if (surplusPercent > 0 && surplusPercent <= 100) {
            surplusText += " (" + surplusPercent + " % used)";
        } else {
            const drawnFromGrid = currentStatus["chargerAverage"] - currentStatus["surplus"];
            if (drawnFromGrid > 0) {
                surplusText += "; " + formatWatt(drawnFromGrid, false) + " from grid";
            }
        }
    }

    if (currentStatus["overrideOnePhaseMinP"] !== null) {
        surplusText += "<br>Override 1-phase charging minimum: "
                       + formatWatt(currentStatus["overrideOnePhaseMinP"], false);
    }

    $("#surplus").html(surplusText);

    // Surplus meter

    $("#low_l").removeClass("meter");
    $("#low_r").removeClass("meter");
    $("#one_phase_l").removeClass("meter");
    $("#one_phase_r").removeClass("meter");
    $("#three_phases").removeClass("meter");
    $("#meter_wrapper").removeClass("meter");
    $("#three_phases").text("");

    if (currentStatus["surplus"] < daemonConfig["onePhaseMinP"]) {
        let lowL = round(currentStatus["surplus"] / maxPv * 100.0);
        if (currentStatus["surplus"] !== null) {
            $("#low_l").addClass("meter");
        }
        $("#low_l").css("width", lowL + "%");
        let lowR = meterLow - lowL;
        $("#low_r").css("width", lowR + "%");

        $("#one_phase_l").css("width", "0%");
        $("#one_phase_r").css("width", meterOnePhase + "%");

        $("#three_phases").css("width", "0%")

    } else if (currentStatus["surplus"] < daemonConfig["threePhasesMinP"]) {
        $("#low_l").css("width", "0%");
        $("#low_r").css("width", meterLow + "%");

        let onePhaseL = round((currentStatus["surplus"] - daemonConfig["onePhaseMinP"])
                              / maxPv * 100.0);
        $("#one_phase_l").css("width", onePhaseL + "%");
        $("#one_phase_l").addClass("meter");

        let onePhaseR = meterOnePhase - onePhaseL;
        $("#one_phase_r").css("width", onePhaseR + "%");

        $("#three_phases").css("width", "0%")

    } else {
        $("#low_l").css("width", "0%");
        $("#low_r").css("width", meterLow + "%");

        $("#one_phase_l").css("width", "0%");
        $("#one_phase_r").css("width", meterOnePhase + "%");

        let threePhases = round((currentStatus["surplus"] - daemonConfig["threePhasesMinP"])
                                / maxPv * 100.0);

        if (threePhases < meterThreePhases) {
            $("#three_phases").css("width", threePhases + "%");
            $("#three_phases").addClass("meter");
        } else if (threePhases == meterThreePhases) {
            $("#three_phases").css("width", meterThreePhases + "%");
            $("#meter_wrapper").addClass("meter");
        } else {
            $("#three_phases").css("width", meterThreePhases + "%");
            $("#three_phases").text("Off Scale >");
        }
    }

    // Controller parameters
    $("#battery_priorized").html(currentStatus["priorizeBattery"] ? "" : " not");
    $("#toggle_battery").html(currentStatus["priorizeBattery"] ? "Disable" : "Enable");
}

// Helper function for displaying the answer of the contol queries below

function displayResponse(text)
{
    clearTimeout(responseTimeoutId);

    clearTimeout(updateTimeoutId);
    update();

    let newText = "[" + formatDate(new Date()) + "] " + text;
    const currentText = $("#response").html();
    if (currentText) {
        newText = currentText + "<br>" + newText;
    }

    $("#response").html(newText);
    $(window).scrollTop(0);

    responseTimeoutId = setTimeout(function() { $("#response").html(""); }, 5000);
}

// Set or calculate an energy limit

function setLimit(limit)
{
    if (limit == -1) {
        const actual = $("#percent_actual").val();
        const nominal = $("#percent_nominal").val();

        // Save the entered values for restoring when we reload the page later
        localStorage.setItem("limitPercentActual", actual);
        localStorage.setItem("limitPercentNominal", nominal);

        if (nominal <= actual) {
            alert("Invalid input for limit calculation:\n"
                + "The nominal value has to be greater than the actual value");
            return;
        }

        limit = capacity / 100.0 * (nominal - actual);
    }

    $.ajax({ url: requestUrl("limit?" + limit), dataType: "html" })

    .fail(function(request, textStatus, errorThrown)
    {
        requestFailed(request.responseText);
    })

    .done(function(response)
    {
        displayResponse(response);
    });
}

// Control function to override the 1-phase charging minimum

function overrideOnePhaseMinimum(override)
{
    let query = "";
    if (override) {
        query = $("#one_phase_p").val();
        // Save the entered value for restoring when we reload the page later
        localStorage.setItem("minimumOnePhaseP", query);
    } else {
        query = "restore";
    }

    $.ajax({ url: requestUrl("minimum?" + query), dataType: "html" })

    .fail(function(request, textStatus, errorThrown)
    {
        requestFailed(request.responseText);
    })

    .done(function(response)
    {
        displayResponse(response);
    });
}

// Control function to trigger a controller restart

function restartController()
{
    $.ajax({ url: requestUrl("restart"), dataType: "html" })

    .fail(function(request, textStatus, errorThrown)
    {
        requestFailed(request.responseText);
    })

    .done(function(response)
    {
        displayResponse(response);
    });
}

// Control function to toggle battery priorization

function toggleBattery()
{
    const text = $("#toggle_battery").html();

    if (text == "Enable") {
        $.ajax({ url: requestUrl("battery?priorize"), dataType: "html" })

        .fail(function(request, textStatus, errorThrown)
        {
            requestFailed(request.responseText);
        })

        .done(function(response)
        {
            displayResponse(response);
        });

    } else if (text == "Disable") {
        $.ajax({ url: requestUrl("battery?unpriorize"), dataType: "html" })

        .fail(function(request, textStatus, errorThrown)
        {
            requestFailed(request.responseText);
        })

        .done(function(response)
        {
            displayResponse(response);
        });
    }
}

// Start or stop the dameon using helper scripts

function initDaemon(command)
{
    if (command != "start" && command != "stop") {
        alert("Unknown command: " + command);
        return;
    }

    $.ajax({ url: "init_daemon.php?" + encodeURIComponent(command), dataType: "html" })

    .fail(function(request, textStatus, errorThrown)
    {
        requestFailed(request.responseText);
    })

    .done(function(response)
    {
        // Display the script output
        $("#response").html(response);

        // If we're stopping: Do an update so that the status will be hidden
        if (command == "stop") {
            clearTimeout(updateTimeoutId);
            update();

        // If we're starting: Try to query the server. If it succeeds, reload to show the status
        } else if (command == "start") {
            setTimeout(function()
            {
                $.getJSON(requestUrl("status"))
                .done(function(json)
                {
                    location.reload();
                });
            }, 2000);
        }
    });
}
