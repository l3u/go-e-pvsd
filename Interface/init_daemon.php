<?php

// SPDX-FileCopyrightText: none
//
// SPDX-License-Identifier: CC0-1.0

// This is a helper wrapper script that starts or stops go-e-pvsd via the machine's init system.
// It's called by the interface with one GET parameter, either "start" or "stop".

// It assumes that one of the shipped init scripts is used to manage go-e-pvsd.
// Feel free to adjust according to your needs, if this is necessary!

// Check the passed GET parameter to be valid

$command = "";

if (! isset($_GET["start"]) && ! isset($_GET["stop"])) {
    echo "Unknown command, pass either \"start\" or \"stop\"\n";
    exit;
} else if (isset($_GET["start"])) {
    $command = "start";
} else if (isset($_GET["stop"])) {
    $command = "stop";
}

// Run the init script

// Be sure to allow this to function. E.g. run visudo and add an entry like
//
//     www-data ALL=(ALL) NOPASSWD: /etc/init.d/go-e-pvsd
//
// with www-data being possibly replaced by whatever user your HTTP server runs as.

passthru("sudo /etc/init.d/go-e-pvsd $command &>/dev/stdout");
