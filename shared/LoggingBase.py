# SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
#
# SPDX-License-Identifier: GPL-3.0-or-later

class LoggingBase:
    def __init__(self, logger):
        self.logger = logger

    def log(self, message: str, level: int = 0):
        self.logger.log(message, level)
