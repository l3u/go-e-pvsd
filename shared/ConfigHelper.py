# SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import os

from configparser import ConfigParser

""" Try to read system-wide, per-user and cwd config files """

class ConfigHelper:
    def __init__(self, logger):
        self.logger = logger

        self.logger.log("Searching for config files", 1)

        self.parsedConfig = ConfigParser()
        # We want case-sensitive keys
        self.parsedConfig.optionxform = lambda option: option
        parsedFiles = self.parsedConfig.read([
            "/etc/go-e-pvsd.ini",
            "{}/.config/go-e-pvsd.ini".format(os.path.expanduser("~")),
            "{}/go-e-pvsd.ini".format(os.getcwd()) ])

        if not parsedFiles:
            self.logger.log("No config files found, using the default settings", 1)
        else:
            self.logger.log("Found the following config file(s) that were parsed, in order:", 1)
            for fileName in parsedFiles:
                self.logger.log("    {}".format(fileName), 1)

    """ Update a given dict of settings with the found ones"""

    def updateConfig(self, config: dict, configTypes: dict, section: str) -> dict:
        self.logger.log("Processing the \"{}\" config section".format(section), 1)

        if not section in self.parsedConfig:
            self.logger.log("No config file contains a \"{}\" section. No values could be "
                            "adopted.".format(section))
        else:
            settings = dict(self.parsedConfig.items(section))
            for key in config.keys():
                if key in settings:
                    config[key] = settings[key]

            extraKeys = list(filter(lambda i: i not in config.keys(), settings.keys()))
            if extraKeys:
                self.logger.log("Warning: The parsed config file(s) contain(s) keys that don't "
                                "exist.")
                self.logger.log("The following key(s) will be ignored:")
                for key in extraKeys:
                    self.logger.log("    {}".format(key))
                self.logger.log("Please review your configuration!")

        # Apply the correct type
        for key in config.keys():
            if configTypes[key] != bool:
                config[key] = configTypes[key](config[key])
            else:
                value = str(config[key]).lower()
                if value == "true" or value == "yes" or value == "on" or value == "1":
                    config[key] = True
                else:
                    config[key] = False

        return config
