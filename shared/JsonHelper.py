# SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import requests

""" Get a JSON answer from a HTTP request """

def jsonQuery(url: str, values: dict, timeout: int) -> (dict, str):
    try:
        answer = requests.get(url, params = values, timeout = timeout)
    except Exception as error:
        return {}, "HTTP request to {} failed: {}".format(url, error)

    try:
        answer.raise_for_status()
    except requests.HTTPError as error:
        return {}, "HTTP response of {} was not okay: {}".format(answer.url, error.response.text)

    try:
        json = answer.json()
    except Exception as error:
        return {}, "Could not parse JSON response from {}: {}".format(answer.url, error)

    return json, None
