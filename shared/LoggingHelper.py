# SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from syslog import syslog
from time import strftime

""" Log to syslog or print out to the console """

class LoggingHelper:
    def __init__(self, logLevel: int, daemonize: bool):
        self.logLevel = logLevel
        self.daemonize = daemonize

    def log(self, message: str, level: int = 0):
        if level > self.logLevel:
            return

        if self.daemonize:
            syslog(message)
        else:
            print("[{}] {}".format(strftime("%Y-%m-%d %H:%M:%S"), message))
