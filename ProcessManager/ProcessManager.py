# SPDX-FileCopyrightText: 2023-2024 Tobias Leupold <tl at stonemx dot de>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import threading
import signal

from http.server import HTTPServer, BaseHTTPRequestHandler
from importlib import import_module

from shared.LoggingHelper import LoggingHelper
from shared.ConfigHelper import ConfigHelper

from Controller.Controller import Controller
from Server.RequestHandler import RequestHandler

class ProcessManager:
    """ This class sets up all classes we need, keeps calling the controller's update() function
        and catches signals that informs the daemon to terminate """

    def __init__(self, args):
        self.config = {
            # The address the controller server will bind to.
            # Use "" here or an empty string ("serverBindAddress =") in an ini file to bind to all
            # interfaces (with 127.0.0.1, the server will only be accessible from the local machine)
            "serverBindAddress": "127.0.0.1",
            # The port the controller server will use
            "serverPort"       : 8008,
            # The inverter backend to use (currently, there's only "FroniusGen24")
            "inverterBackend"  : "FroniusGen24",
            # The update interval
            "heartbeat"        : 3
        }

        self.configTypes = {
            "serverBindAddress": str,
            "serverPort"       : int,
            "inverterBackend"  : str,
            "heartbeat"        : int
        }

        # The parsed command line arguments from the startup script
        self.args = args

        # The Objects we will initialize
        self.logger = None
        self.configHelper = None
        self.inverterBackend = None
        self.controller = None
        self.timer = None
        self.server = None

        # Signal catched indicator
        self.signalCatched = False

        # Event the main script will wait for in daemon mode
        self.finished = threading.Event()

    def log(self, message, level = 0):
        """ Log to stdout or syslog helper """
        self.logger.log(message, level)

    def setup(self) -> bool:
        # First setup the logger
        self.logger = LoggingHelper(1 if self.args.v else 0, self.args.d)

        self.log("Setting up the go-e PV Surplus Daemon")

        # Read all possible config files
        self.configHelper = ConfigHelper(self.logger)

        # Update the config with the found settings
        self.config = self.configHelper.updateConfig(self.config, self.configTypes, "main")

        # Setup the inverter backend

        self.log("Setting up the \"{}\" inverter backend".format(
                 self.config["inverterBackend"]), 1)

        try:
            InverterBackend = import_module("InverterBackends.{}".format(
                                            self.config["inverterBackend"]))
        except Exception as error:
            self.log("Could not load the \"{}\" inverter backend!".format(
                     self.config["inverterBackend"]))
            self.log("The error was: {}".format(error))
            self.log("Can't start up!")
            return False

        self.inverterBackend = InverterBackend.Backend(self.logger, self.configHelper)

        # Setup the controller

        self.log("Setting up the controller", 1)
        self.controller = Controller(self.logger, self.configHelper, self.inverterBackend)

        if not self.controller.cacheSettings():
            # At least try to restore the settings
            self.controller.restoreSettings()
            return False

        # Setup the server

        try:
            # This is a HTTPServer that uses the custom RequestHandler class to answer GET requests.
            # The RequestHandler has to call some of the controller's functions, which are passed to
            # it using the below lambda construct (which seems to be a quite odd way to do this, at
            # least for me as a C++ guy, but that's apparently that "pythonic" thingy everybody
            # talks about ;-)
            self.server = HTTPServer((self.config["serverBindAddress"], self.config["serverPort"]),
                                     lambda *_: RequestHandler(self.controller, *_))
        except Exception as error:
            self.log("Could not start the controller server!")
            self.log("The error was: {}".format(error))
            self.log("Can't start up!")
            return False

        # All done
        return True

    def start(self):
        self.log("Starting the server")
        thread = threading.Thread(target = self.server.serve_forever)
        thread.start()

        self.log("Starting the controller timer")
        self.scheduleNextRun()

    def scheduleNextRun(self):
        if self.signalCatched:
            # Prevent the timer from starting again if we're about to stop
            return

        # Keep calling the controller's update() function
        self.controller.update()
        self.timer = threading.Timer(self.config["heartbeat"], self.scheduleNextRun)
        self.timer.start()

    def terminate(self, signum, frame):
        self.log("Received {}".format(signal.Signals(signum).name))

        self.signalCatched = True
        self.controller.stopCharging()
        self.controller.stop(stopReason = "Received terminate signal")
        self.controller.restoreSettings()
        self.controller.checkReEnableBattery()

        if self.timer:
            self.log("Stopping the controller timer")
            self.timer.cancel()

        self.log("Stopping the server")
        self.server.shutdown()

        self.log("Will now terminate")
        self.finished.set()
