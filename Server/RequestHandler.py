# SPDX-FileCopyrightText: 2023-2024 Tobias Leupold <tl at stonemx dot de>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from http.server import BaseHTTPRequestHandler
from urllib.parse import urlparse

class RequestHandler(BaseHTTPRequestHandler):
    """ Request handler to toggle the controller and get it's status """

    def __init__(self, controller, *args, **kwargs):
        """ Cache a controller pointer and initialize the base class """
        self.controller = controller
        super().__init__(*args, **kwargs)

    def answer(self, contentType: str, content: str):
        """ Prepare and send the answer to a GET request """

        payload = bytes(content, "utf-8")

        self.send_response(200)
        self.send_header("Content-Type", "{}; charset=UTF-8".format(contentType))
        self.send_header("Content-Length", len(payload))
        self.end_headers()

        self.wfile.write(payload)

    def do_GET(self):
        """ Process a GET request """

        # Derive the command from the path requested in the URL
        parsed = urlparse(self.path)
        command = parsed.path.replace("/", "")
        query = parsed.query

        if command == "status":
            # We want to get the controller's current status
            self.answer("application/json", self.controller.status())

        elif command == "config":
            # We want to get the controller configuration
            self.answer("application/json", self.controller.configJson)

        elif command == "restart":
            # We want to restart the controller
            self.answer("text/plain", self.controller.restart())

        elif command == "minimum":
            # We want to override or reset the 1-phase charging minimum

            if query == "restore":
                # We want to reset the minimum to the config value
                self.controller.config["onePhaseMinP"] = self.controller.originalOnePhaseMinP
                text = "Restored {} W as the 1-phase charging " \
                       "minimum".format(self.controller.originalOnePhaseMinP)
                self.controller.log(text)
                self.answer("text/plain", text)

            else:
                # We want to override the config value

                valueParsed = True
                value = 0

                try:
                    value = int(query)
                except:
                    valueParsed = False

                if not valueParsed or value < 0:
                    self.answer("text/plain", "Invalid minimum parameter: \"{}\"".format(query))
                    return

                self.controller.config["onePhaseMinP"] = value
                text = "Set a 1-phase charging minimum override value of {} W".format(value)
                self.controller.log(text)
                self.answer("text/plain", text)

        elif command == "battery":
            # We want to (un)priorize battery charging

            if query == "priorize":
                self.controller.config["priorizeBattery"] = True

                # If the controller is currently running, we have to restart it
                if self.controller.started:
                    self.controller.stop("Changing battery priorization")
                    self.controller.start()

                self.controller.log("Priorized battery charging over car charging")
                self.answer("text/plain", "Priorized battery charging over car charging")

            elif query == "unpriorize":
                # We want to priorize car charging over battery charging
                self.controller.config["priorizeBattery"] = False
                self.controller.log("Priorized car charging over battery charging")
                self.answer("text/plain", "Priorized car charging over battery charging")

            else:
                # An unknown parameter was given
                self.answer("text/plain", "Unknown battery parameter: \"{}\"".format(query))

        elif command == "limit":
            # We want to set an energy limit

            limitParsed = True
            limit = 0.0

            try:
                limit = float(query)
            except:
                limitParsed = False

            if not limitParsed or limit < 0:
                self.answer("text/plain", "Invalid limit parameter: \"{}\"".format(query))
                return

            self.answer("text/plain", self.controller.setLimit(limit))

        else:
            # An unknown command was given
            self.answer("text/plain", "Unknown command: \"{}\"".format(command))

    def log_message(self, format, *args):
        # Disable logging
        pass
