# SPDX-FileCopyrightText: 2023-2024 Tobias Leupold <tl at stonemx dot de>
#
# SPDX-License-Identifier: GPL-3.0-or-later

class Averager:

    """ Provide an average over a defined number of values """

    def __init__(self, count: int):
        self.bufferLength = count
        self.buffer = []
        self.average = None

    def update(self, value):
        self.buffer.append(value)
        while len(self.buffer) > self.bufferLength:
            self.buffer.pop(0)

        values = len(self.buffer)
        if values > 0:
            self.average = sum(self.buffer) / values
        else:
            self.average = None

    def last(self):
        if len(self.buffer) > 0:
            return self.buffer[-1]
        else:
            return None

    def reset(self):
        self.buffer = []
        self.average = None
