# SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
#
# SPDX-License-Identifier: GPL-3.0-or-later

class Timespan:
    """ Helper class to measure a timespan """

    def __init__(self, timeProvider):
        self.now = timeProvider
        self.time = None
        self.secondsPassed = 0

    def start(self):
        """ Start measuring a the timespan """
        self.time = self.now()

    def stop(self):
        """ Invalidate the timespan """
        self.time = None
        self.secondsPassed = 0

    def update(self):
        """ Update the seconds passed since the start point if we have one """
        if self.time is None:
            return
        self.secondsPassed = int((self.now() - self.time).total_seconds())

    def __bool__(self) -> bool:
        """ Return True if a start point was set, else False """
        return self.time is not None

    def __call__(self) -> int:
        """ Return the seconds passed since the start point """
        return self.secondsPassed
