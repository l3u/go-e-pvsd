# SPDX-FileCopyrightText: 2023 Tobias Leupold <tl at stonemx dot de>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from datetime import datetime

class TimeProvider:

    """ Small helper to have a shareable datetime object """

    def __init__(self):
        self.time = datetime.now()

    def update(self):
        """ Set the cached time to the current time """
        self.time = datetime.now()

    def __call__(self):
        """ Return the cached time """
        return self.time
