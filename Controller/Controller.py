# SPDX-FileCopyrightText: 2023-2024 Tobias Leupold <tl at stonemx dot de>
#
# SPDX-License-Identifier: GPL-3.0-or-later

from shared.LoggingBase import LoggingBase
from shared.JsonHelper import jsonQuery

from Controller.Averager import Averager
from Controller.TimeProvider import TimeProvider
from Controller.Timespan import Timespan

from json import dumps as formatJson
from datetime import datetime
from math import floor

class Controller(LoggingBase):
    """ The actual surplus charging controller """

    def __init__(self, logger, configHelper, inverterBackend):
        # Pass the logger to the base class
        super().__init__(logger)

        """ Initialize the controller """

        """
        The go-e-Charger API keys we will be using are:
        "modelStatus", "car", "frc", "psm", "amp", "nrg", "eto" and "dwo".

        - "modelStatus" is the "Reason why we allow charging or not right now"

        - "car" ("carState") reflects if a car is connected and what is it's charging state

        - "frc" ("forceState") allows or forbids charging.

        - "psm" ("phaseSwitchMode") defines if we want to charge with one or three phases.

        For those, we have named possible parameters:
        """

        self.paramDesc = {
            "modelStatus": {
                 0: "NotChargingBecauseNoChargeCtrlData",
                 1: "NotChargingBecauseOvertemperature",
                 2: "NotChargingBecauseAccessControlWait",
                 3: "ChargingBecauseForceStateOn",
                 4: "NotChargingBecauseForceStateOff",
                 5: "NotChargingBecauseScheduler",
                 6: "NotChargingBecauseEnergyLimit",
                 7: "ChargingBecauseAwattarPriceLow",
                 8: "ChargingBecauseAutomaticStopTestLadung",
                 9: "ChargingBecauseAutomaticStopNotEnoughTime",
                10: "ChargingBecauseAutomaticStop",
                11: "ChargingBecauseAutomaticStopNoClock",
                12: "ChargingBecausePvSurplus",
                13: "ChargingBecauseFallbackGoEDefault",
                14: "ChargingBecauseFallbackGoEScheduler",
                15: "ChargingBecauseFallbackDefault",
                16: "NotChargingBecauseFallbackGoEAwattar",
                17: "NotChargingBecauseFallbackAwattar",
                18: "NotChargingBecauseFallbackAutomaticStop",
                19: "ChargingBecauseCarCompatibilityKeepAlive",
                20: "ChargingBecauseChargePauseNotAllowed",
                22: "NotChargingBecauseSimulateUnplugging",
                23: "NotChargingBecausePhaseSwitch",
                24: "NotChargingBecauseMinPauseDuration"
            },

            "car": {
                0: "Unknown/Error",
                1: "Idle",
                2: "Charging",
                3: "WaitCar",
                4: "Complete",
                5: "Error"
            },

            "frc": {
                0: "Neutral",
                1: "Off",
                2: "On"
            },

            "psm": {
                0: "Auto",
                1: "Force_1",
                2: "Force_3"
            },
        }

        """
        - "amp" ("requestedCurrent") represents the allowed charging current in ampere.
          The values can be integers from 6 to 16.

        The other keys are read only:

        - "nrg" ("energy array") provides information about the current energy used by the charger.
          The following indices exist:
               0: U L1 -.
               1: U L2  | <-- These represent the current present voltage
               2: U L3 -'
               3: U N
               4: I L1
               5: I L2
               6: I L3
               7: P L1
               8: P L2
               9: P L3
              10: P N
              11: P Total <-- This one is the power currently consumed by the charger
              12: pf L1
              13: pf L2
              14: pf L3
              15: pf N

        - "eto" ("energy_total") represents the total Wh ever charged. There's also "wh", which
          returns the "energy since car connected", but this seems to be unreliable: Apparently
          depending on how long the charging was disabled, this value is reset to 0, even if the
          car actually wasn't disconnected. Thus, we cache the value at startup and calculate the
          charged energy by the current value compared to it.

        - "dwo" ("charging energy limit") is either None or the set energy limit
        """

        self.logger = logger
        self.inverterBackend = inverterBackend

        self.config = {
            # The go-e Charger's IP address
            "chargerIp"                : "192.168.1.100",
            # HTTP request timeout (seconds to wait)
            "requestTimeout"           : 10,
            # Number of measured values to average
            "averaging"                : 5,
            # Minimum W for one-phase charging
            "onePhaseMinP"             : 1400,
            # Minimum W for three-phases charging
            "threePhasesMinP"          : 4200,
            # Set to True to exclude the battery charging current from the surplus calculation
            "priorizeBattery"          : False,
            # Set to True to prevent drawing energy from the house battery when charging
            "disableBattery"           : False,
            # Fraction of the surplus to use for current calculation
            "useSurplusFraction"       : 0.97,
            # Interval for checking if a car is connected
            "checkCarConnectedInterval": 60,
            # Seconds to wait before adjusting the ampere when charging is currently going on
            "ampereChangeHoldTime"     : 20,
            # Minimum charging time when one-phase charging was started
            "onePhaseMinChargeTime"    : 60,
            # Time to wait after starting one-phase charging before adjusting the ampere
            "onePhaseAmpHoldTime"      : 30,
            # Minimum charging time when three-phase charging was started
            "threePhasesMinChargeTime" : 60,
            # Time to wait after starting three-phases charging before adjusting the ampere
            "threePhasesAmpHoldTime"   : 40,
            # Minimum time to wait after stopping charging to start again
            "chargingPauseHoldTime"    : 60
        }

        self.configTypes = {
            "chargerIp"                : str,
            "requestTimeout"           : int,
            "averaging"                : int,
            "onePhaseMinP"             : int,
            "threePhasesMinP"          : int,
            "priorizeBattery"          : bool,
            "disableBattery"           : bool,
            "useSurplusFraction"       : float,
            "checkCarConnectedInterval": int,
            "ampereChangeHoldTime"     : int,
            "onePhaseMinChargeTime"    : int,
            "onePhaseAmpHoldTime"      : int,
            "threePhasesMinChargeTime" : int,
            "threePhasesAmpHoldTime"   : int,
            "chargingPauseHoldTime"    : int
        }

        # Overwrite the default configuration with config file values
        self.config = configHelper.updateConfig(self.config, self.configTypes, "Controller")

        # Cache the charging minimum (it can be changed run-time)
        self.originalOnePhaseMinP = self.config["onePhaseMinP"]

        # Cache a JSON version of the config
        self.configJson = formatJson(self.config, indent = 4)

        # Charger settings cached at startup
        self.cachedSettings = {}

        # Setup the averagers
        self.grid               = Averager(self.config["averaging"])
        self.battery            = Averager(self.config["averaging"])
        self.charger            = Averager(self.config["averaging"])
        self.voltageOnePhase    = Averager(self.config["averaging"])
        self.voltageThreePhases = Averager(self.config["averaging"])

        # Controller started indicator
        self.started = False
        self.stopReason = "Car disconnected"

        # Energy counter and limit
        self.energyOffset = 0.0
        self.energyLimit = None

        # Current values
        self.lastUpdate         = None
        self.limitRefreshed     = None
        self.carConnected       = None
        self.carReconnected     = False
        self.currentAmpere      = 0
        self.timeCharged        = 0
        self.chargedEnergy      = 0.0
        self.energyLimitReached = False
        self.surplus            = None
        self.batteryDisabled    = False

        # Setup the timespan measurement helpers

        self.now = TimeProvider()

        self.checkCarConnected          = Timespan(self.now)
        self.chargingStartedThreePhases = Timespan(self.now)
        self.chargingStartedOnePhase    = Timespan(self.now)
        self.lastAmpereChange           = Timespan(self.now)
        self.minimumReachedThreePhases  = Timespan(self.now)
        self.minimumReachedOnePhase     = Timespan(self.now)
        self.minimumUndercutThreePhases = Timespan(self.now)
        self.minimumUndercutOnePhase    = Timespan(self.now)
        self.chargingStopped            = Timespan(self.now)

        self.chargingTimespans = [
            self.chargingStartedThreePhases,
            self.chargingStartedOnePhase,
            self.lastAmpereChange,
            self.minimumReachedThreePhases,
            self.minimumReachedOnePhase,
            self.minimumUndercutThreePhases,
            self.minimumUndercutOnePhase
        ]

        # We handle self.checkCarConnected and self.chargingStopped separately

        # Start the charging stopped timespan
        self.chargingStopped.start()

    def getChargerValues(self, values: list) -> (bool, dict):
        """ Get values from the charger """

        answer, error = jsonQuery("http://{}/api/status".format(self.config["chargerIp"]),
                                  { "filter": ",".join(values) }, self.config["requestTimeout"])
        if error:
            self.log(error)
            return False, {}

        for key in values:
            if not key in answer:
                self.log("Did not receive the requested key \"{}\"".format(key))
                self.log("The charger's answer was: {}".format(answer))
                return False, {}

        return True, answer

    def setChargerValues(self, values: dict) -> bool:
        """ Set charger values """

        answer, error = jsonQuery("http://{}/api/set".format(self.config["chargerIp"]),
                                  values, self.config["requestTimeout"])
        if error:
            self.log(error)
            return False

        for key in values.keys():
            if key in answer and answer[key]:
                value = values[key]
                if key in self.paramDesc:
                    value = "{} ({})".format(value, self.paramDesc[key][value])
                self.log("    set {} to {}".format(key, value), 1)
            else:
                self.log("Failed to set {}!".format(key))
                self.log("The charger's answer was: {}".format(answer))
                return False

        return True

    def calculateAmpere(self, forcePhases = False) -> int:
        """ Calculate the ampere we can charge with """

        # We need an int value for the ampere to charge with.

        if forcePhases:
            if forcePhases == 1 or forcePhases == 3:
                phases = forcePhases
            else:
                # This should not happen
                self.log("Invalid value for forcePhases given!")
                return 6
        else:
            if self.chargingStartedOnePhase:
                phases = 1
            elif self.chargingStartedThreePhases:
                phases = 3
            else:
                # We don't charge and no forcePhases value was given
                # This shouldn't happen
                self.log("Ampere calculation requested, but we currently don't charge")
                return 6

        divisor = self.voltageOnePhase.average if phases == 1 else self.voltageThreePhases.average
        if divisor is None or divisor == 0:
            self.log("No present current voltages, assuming 230 V")
            divisor = 230 if phases == 1 else 3 * 230

        # Python's // operator does floor the division's result of two floats, but still returns a
        # float and not an int. Thus, we can either use math.floor() or we could explicitely cast
        # the result to int. Imo, math.floor() expresses what we want here better :-)
        ampere = floor(self.surplus * self.config["useSurplusFraction"] / divisor)

        # We need at least 6 A to charge, and can charge with at most 16
        return min(max(6, ampere), 16)

    def updateAmpere(self):
        """ Update the current ampere setting """

        # Check if the minimum ampere hold time has passed

        secsToGo = 0
        if self.chargingStartedOnePhase:
            secsToGo = self.config["onePhaseAmpHoldTime"] - self.chargingStartedOnePhase()
        elif self.chargingStartedThreePhases:
            secsToGo = self.config["threePhasesAmpHoldTime"] - self.chargingStartedThreePhases()
        else:
            # This shouldn't be called when no charging is going on, but who knows ;-)
            self.log("Charging is not started, not adjusting the charge current", 1)
            return

        if secsToGo > 0:
            self.log("Waiting {} secs before adjusting the charge current".format(secsToGo), 1)
            return

        # Check if the last change to amp was long enough ago
        if self.lastAmpereChange() < self.config["ampereChangeHoldTime"]:
            return

        # Check if we actually have to change the amp value
        ampere = self.calculateAmpere()
        if self.currentAmpere == ampere:
            return

        # Update the amp value
        self.log("Adjusting the charge current to {} A".format(ampere), 1)
        self.setChargerValues({ "amp": ampere })
        self.currentAmpere = ampere
        self.lastAmpereChange.start()

    def updateCarConnected(self) -> bool:
        self.log("Checking if a car is connected", 1)

        success, answer = self.getChargerValues([ "car" ])
        if not success:
            self.log("Could not check the current car state!")
            return False

        oldCarConnected = self.carConnected

        car = self.paramDesc["car"][answer["car"]]
        self.log("    Car state: {}".format(car), 1)
        self.carConnected = car == "WaitCar" or car == "Charging" or car == "Complete"

        if oldCarConnected is not None and not oldCarConnected and self.carConnected:
            self.carReconnected = True

        return True

    def refreshLimit(self) -> bool:
        """ Get the current energy counter and limit """

        self.log("Refreshing the energy counter and limit")

        success, answer = self.getChargerValues([ "dwo", "eto" ])
        if not success:
            self.log("    Failed to get the energy counter and limit!")
            return False

        self.energyLimit = answer["dwo"]
        if self.energyLimit is None:
            self.log("    No energy limit defined", 1)
        else:
            self.log("    Defined energy limit: {:.1f} kWh".format(self.energyLimit / 1000.0), 1)

        self.energyOffset = answer["eto"]
        self.log("    Charged energy counter: {:.1f} kWh".format(self.energyOffset / 1000.0), 1)

        self.timeCharged = 0
        self.chargedEnergy = 0.0
        self.energyLimitReached = False

        self.now.update()
        self.limitRefreshed = self.now()

        return True

    def checkDisableCharging(self) -> bool:
        if self.chargingStartedOnePhase or self.chargingStartedThreePhases:
            # We're charging anyway at the moment. No need to disable charging.
            return True
        else:
            # The model status could have been reset meanwhile (e.g. by disconnecting the car and
            # connecting it again). We have to be sure that we're in control if the car can start
            # charging or not.
            return self.setChargerValues({ "frc": 1 })

    def restart(self) -> str:
        """ Restart the controller and refresh the energy limit """

        self.log("Controller restart requested")

        success, answer = self.getChargerValues([ "modelStatus" ])
        if not success:
            answer = "Could not get the current model status. Can't restart!"
            self.log(answer)
            return answer

        if self.paramDesc["modelStatus"][answer["modelStatus"]] == "NotChargingBecauseEnergyLimit":
            answer = "The model status is currently \"Not charging because energy limit\". " \
                     "Can't restart!"
            self.log(answer)
            return answer

        if not self.refreshLimit():
            answer = "Could not refresh the energy counter and limit! Won't restart!"
            self.log(answer)
            return answer

        if not self.checkDisableCharging():
            answer = "Failed to disable charging! Won't restart!"
            self.log(answer)
            return answer

        self.resetCurrentValues()
        self.timeCharged = 0
        self.start()

        answer = "Restarted the controller"
        self.log(answer)
        return answer

    def setLimit(self, limit: float) -> str:
        """ Refresh the energy counter and set an energy limit """

        if limit > 0:
            self.log("Setting the energy limit to {:.1f} kWh requested".format(limit / 1000.0))
        else:
            self.log("Removing the energy limit requested")

        # self.refreshLimit() will set self.energyLimitReached to False. Thus, we cache it here:
        energyLimitWasReached = self.energyLimitReached

        if not self.refreshLimit():
            answer = "Could not refresh the energy limit. Can't set a new one!"
            self.energyLimitReached = energyLimitWasReached
            self.log(answer)
            return answer

        if self.energyLimit is not None:
            answer = "Energy limit ({:.1f} kWh) set by the charger. Won't overwrite it.".format(
                      self.energyLimit / 1000.0)
            self.energyLimitReached = energyLimitWasReached
            self.log(answer)
            return answer

        if not self.checkDisableCharging():
            answer = "Failed to disable charging! Can't set a new limit!"
            self.energyLimitReached = energyLimitWasReached
            self.log(answer)
            return answer

        if limit > 0:
            self.energyLimit = limit
            answer = "Set the energy limit to {:.1f} kWh".format(self.energyLimit / 1000.0)
        else:
            self.energyLimit = None
            answer = "Removed the energy limit"

        # Reset all timespans
        self.resetCurrentValues()

        # If we reached the last energy limit: Possibly restart the controller

        if energyLimitWasReached:
            self.log("    Resetting the \"energy limit reached\" indicator", 1)

            # Check if a car is connected

            # If this fails, the update loop will check for a car being connected later and start
            # the controller again anyway due to self.energyLimitReached being False again now.

            if self.updateCarConnected():
                self.checkCarConnected.start()

                if self.carConnected:
                    # A car is connected, we can directly start to watch PV surplus again.
                    self.log("    Starting to watch PV surplus again", 1)
                    self.start()
                else:
                    # We switch from "Enery limit reached" to "Car disconnected" as the stop
                    # reason. The update loop will check for a car being connected again, due to
                    # self.energyLimitReached being False again now.
                    self.log("    Waiting for a car being connected again", 1)
                    self.stopReason = "Car disconnected"

        self.log(answer, 1)
        return answer

    def cacheSettings(self) -> bool:
        """ Cache the charger's settings and disable charging """

        self.log("Caching the current charger settings", 1)

        success, answer = self.getChargerValues([ "psm", "amp", "frc" ])
        if not success:
            self.log("Could not cache the current charger parameters!")
            return False

        self.log("    psm: {} ({})".format(answer["psm"], self.paramDesc["psm"][answer["psm"]]), 1)
        self.log("    amp: {}".format(answer["amp"]), 1)
        self.log("    frc: {} ({})".format(answer["frc"], self.paramDesc["frc"][answer["frc"]]), 1)

        self.cachedSettings["psm"] = answer["psm"]
        self.cachedSettings["amp"] = answer["amp"]
        self.cachedSettings["frc"] = answer["frc"]

        # Reset the energy counter and query for an energy limit
        if not self.refreshLimit():
            return False

        # Disable charging to begin
        self.log("Disabling charging")
        if not self.setChargerValues({ "frc": 1 }):
            self.log("Could not disable charging!")
            return False

        return True

    def restoreSettings(self):
        """ Restore the charger settings cached at startup """

        if    not "psm" in self.cachedSettings \
           or not "amp" in self.cachedSettings \
           or not "frc" in self.cachedSettings:

               self.log("Can't restore charger settings: No settings cached!")
               return

        self.log("Restoring the charger settings cached at startup")

        success = self.setChargerValues({ "psm": self.cachedSettings["psm"],
                                          "amp": self.cachedSettings["amp"],
                                          "frc": self.cachedSettings["frc"] })
        if not success:
            self.log("Could not restore the charger settings!")

    def start(self) -> bool:
        """ Start monitoring PV surplus """

        # Check if we're already started
        if self.started:
            self.log("PV surplus monitoring is already running")
            return True

        # Check if a car is connected
        if not self.carConnected:
            self.log("No car is connected. Won't start PV surplus monitoring")
            return False

        self.log("Starting PV surplus monitoring")
        self.started = True
        self.stopReason = None

        return True

    def resetCurrentValues(self):
        # Reset all averagers
        self.grid.reset()
        self.battery.reset()
        self.charger.reset()
        self.voltageOnePhase.reset()
        self.voltageThreePhases.reset()

        # Unset the current (calculated) surplus
        self.surplus = None

        # Reset all active timers
        for timer in self.chargingTimespans:
            if timer:
                timer.start()

    def stop(self, stopReason: str):
        """ Stop monitoring PV surplus """

        # Check if we're running
        if not self.started:
            self.log("PV surplus monitoring is already stopped")
            return

        self.log("Stopping PV surplus monitoring")

        # Reset the charging timespans
        for timespan in self.chargingTimespans:
            timespan.stop()

        # Start the charging stopped one
        self.chargingStopped.start()

        # Indicate that we stopped
        self.started = False
        self.stopReason = stopReason

        self.resetCurrentValues()

    def checkReEnableBattery(self):
        if not self.config["disableBattery"] or not self.batteryDisabled:
            return

        self.log("Re-enabling drawing energy from the house battery")
        if self.inverterBackend.disableBattery(False):
            self.batteryDisabled = False

    def formatTime(self, time) -> str:
        if time is not None:
            return time.astimezone().isoformat(timespec = "seconds")
        else:
            return "n/a"

    def formatTimeSpan(self, timeSpan):
        return timeSpan() if timeSpan else False

    def getCurrentTimeCharged(self):
        if self.chargingStartedOnePhase:
            return self.timeCharged + self.chargingStartedOnePhase()
        elif self.chargingStartedThreePhases:
            return self.timeCharged + self.chargingStartedThreePhases()
        else:
            return self.timeCharged

    def status(self) -> str:
        return formatJson({
            "timestamp"                 : self.formatTime(datetime.now()),
            "started"                   : self.started,
            "priorizeBattery"           : self.config["priorizeBattery"],
            "overrideOnePhaseMinP"      : self.config["onePhaseMinP"] if self.originalOnePhaseMinP \
                                              != self.config["onePhaseMinP"] else None,
            "stopReason"                : self.stopReason,
            "lastUpdate"                : self.formatTime(self.lastUpdate),
            "energyLimit"               : self.energyLimit,
            "limitRefreshed"            : self.formatTime(self.limitRefreshed),
            "timeCharged"               : self.getCurrentTimeCharged(),
            "chargedEnergy"             : self.chargedEnergy,
            "carConnected"              : self.carConnected,
            "currentAmpere"             : self.currentAmpere,
            "surplus"                   : self.surplus,
            "grid"                      : self.grid.last(),
            "gridAverage"               : self.grid.average,
            "battery"                   : self.battery.last(),
            "batteryAverage"            : self.battery.average,
            "charger"                   : self.charger.last(),
            "chargerAverage"            : self.charger.average,
            "chargingStartedThreePhases": self.formatTimeSpan(self.chargingStartedThreePhases),
            "chargingStartedOnePhase"   : self.formatTimeSpan(self.chargingStartedOnePhase),
            "lastAmpereChange"          : self.formatTimeSpan(self.lastAmpereChange),
            "minimumReachedThreePhases" : self.formatTimeSpan(self.minimumReachedThreePhases),
            "minimumReachedOnePhase"    : self.formatTimeSpan(self.minimumReachedOnePhase),
            "minimumUndercutThreePhases": self.formatTimeSpan(self.minimumUndercutThreePhases),
            "minimumUndercutOnePhase"   : self.formatTimeSpan(self.minimumUndercutOnePhase),
            "chargingStopped"           : self.formatTimeSpan(self.chargingStopped)
        }, indent = 4)

    def powerDisplay(self, average: float, current: float, sign: bool) -> str:
        """ Format average and current power """

        averageStr = ""
        currentStr = ""

        if sign:
            if average == 0:
                averageStr = "{:.0f}".format(average)
            else:
                averageStr = "{:+.0f}".format(average)

            if current == 0:
                currentStr = "{:.0f}".format(current)
            else:
                currentStr = "{:+.0f}".format(current)

        else:
            averageStr = "{:.0f}".format(average)
            currentStr = "{:.0f}".format(current)

        return "{} (c: {}) W".format(averageStr, currentStr)

    def stopCharging(self) -> bool:
        self.log("Stopping charging")

        if not self.setChargerValues({ "frc": 1 }):
            self.log("Failed to disable charging!")
            return False

        if self.chargingStartedOnePhase:
            self.timeCharged += self.chargingStartedOnePhase()
            self.chargingStartedOnePhase.stop()

        if self.chargingStartedThreePhases:
            self.timeCharged += self.chargingStartedThreePhases()
            self.chargingStartedThreePhases.stop()

        self.chargingStopped.start()
        self.lastAmpereChange.start()

        return True

    def startChargingOnePhase(self):
        self.log("Starting one-phase charging")

        self.currentAmpere = self.calculateAmpere(forcePhases = 1)
        if not self.setChargerValues({ "psm": 1,
                                       "amp": self.currentAmpere,
                                       "frc": 0 }):
            self.log("Failed to start one-phase charging!")
            return

        if self.chargingStartedThreePhases:
            self.timeCharged += self.chargingStartedThreePhases()
            self.chargingStartedThreePhases.stop()

        self.chargingStartedOnePhase.start()

        self.chargingStopped.stop()
        self.lastAmpereChange.start()

    def startChargingThreePhases(self):
        self.log("Starting three-phases charging")

        self.currentAmpere = self.calculateAmpere(forcePhases = 3)
        if not self.setChargerValues({ "psm": 2,
                                       "amp": self.currentAmpere,
                                       "frc": 0 }):
            self.log("Failed to start three-phase charging!")
            return

        if self.chargingStartedOnePhase:
            self.timeCharged += self.chargingStartedOnePhase()
            self.chargingStartedOnePhase.stop()

        self.chargingStartedThreePhases.start()
        self.chargingStopped.stop()
        self.lastAmpereChange.start()

    def update(self):
        """ Process the current PV and charger data """

        # Update the time provider and the "last update" indicator
        self.now.update()
        self.lastUpdate = self.now()

        # Check if we should check if a car is connected

        self.checkCarConnected.update()
        if self.carConnected is None \
           or self.checkCarConnected() >= self.config["checkCarConnectedInterval"]:

            if not self.updateCarConnected():
                return

            self.checkCarConnected.start()

        # If a car reconnect happened, we disable charging no matter what to make sure we're in
        # control. E.g. the energy limit could have been reached, but the charger is reset due to
        # the unplugging and the car would then begin to charge again nevertheless.
        if self.carReconnected:
            self.log("Car was reconnected. Disabling charging to make sure we're in control")
            if self.checkDisableCharging():
                self.carReconnected = False

        # Check if we should start or stop the controller due to the car connected state

        if self.started and not self.carConnected:
            self.log("Car was disconnected, stopping the controller")

            if not self.stopCharging():
                # Communication with the charger failed -- try again in the next update run
                return

            self.stop(stopReason = "Car disconnected")
            return

        elif not self.started and self.carConnected and not self.energyLimitReached:
            self.log("Car was connected, starting the controller")
            self.start()

        # In each case, we have to update the charging stopped timespan, in case we're not
        # charging right now. The Timespan class will ignore the update if we're not started.
        self.chargingStopped.update()

        # Check if we should disable battery discharging.

        # This is meaningful if we're charging grid-assisted (with a smaller one-phase charging
        # override value). Otherwise, the battery would only buffer away fluctuations until the
        # charging current is adjusted again, which is okay.

        if self.config["disableBattery"] \
           and self.config["onePhaseMinP"] < self.originalOnePhaseMinP \
           and (self.chargingStartedOnePhase or self.chargingStartedThreePhases) \
           and not self.batteryDisabled:

            self.log("Disabling drawing energy from the house battery")
            if self.inverterBackend.disableBattery(True):
                self.batteryDisabled = True

        # Check if we should re-enable battery discharging

        elif self.batteryDisabled \
           and not self.chargingStartedOnePhase \
           and not self.chargingStartedThreePhases:

            self.checkReEnableBattery()

        # Do nothing else if we're not started
        if not self.started:
            return

        # -------------------------------------- Collect data --------------------------------------

        # Update the charging timespans
        for timespan in self.chargingTimespans:
            timespan.update()

        # Get the current grid and battery feed-in or draw.
        #
        # grid    < 0: We feed energy to the grid and have a surplus (current into the grid)
        #         > 0: We draw energy from the grid
        #
        # battery < 0: The battery is being charged (current into the battery)
        #         > 0: The battery discharges and supports the consumption
        success, grid, battery = self.inverterBackend.getPowerData()
        if not success:
            self.log("Failed to get the current inverter data! Assuming no surplus")
        self.grid.update(grid)
        self.battery.update(battery)

        # Get the power consumed by the charger and the energy counter.
        # If we're charging, the charger's enerhy is a part of the actual surplus.
        # Also check if the charger finished charging due to the energy limit being reached

        success, answer = self.getChargerValues([ "nrg", "eto", "modelStatus" ])
        if not success:
            return

        self.voltageOnePhase.update(answer["nrg"][0])
        self.voltageThreePhases.update(sum(answer["nrg"][0:3]))
        self.charger.update(answer["nrg"][11])
        self.chargedEnergy = answer["eto"] - self.energyOffset

        # Check for the energy limit

        if self.paramDesc["modelStatus"][answer["modelStatus"]] == "NotChargingBecauseEnergyLimit" \
           or (self.energyLimit is not None and self.chargedEnergy >= self.energyLimit):

            self.log("Energy limit reached. Stopping the controller")

            if not self.stopCharging():
                # Communication with the charger failed -- try again in the next update run
                return

            self.energyLimitReached = True
            self.stop(stopReason = "Energy limit reached")
            return

        # Calculate the actual surplus

        self.surplus = self.grid.average * -1.0 + self.charger.average

        if not self.config["priorizeBattery"] or self.battery.average > 0:
            self.surplus -= self.battery.average

        if self.surplus < 0:
            self.surplus = 0.0

        # Log the current status if we're verbose
        self.log("Grid: {}; Battery: {}; Charger: {}".format(
                 self.powerDisplay(self.grid.average, self.grid.last(), True),
                 self.powerDisplay(self.battery.average, self.battery.last(), True),
                 self.powerDisplay(self.charger.average, self.charger.last(), False)),
                 1)
        if not self.chargingStartedOnePhase and not self.chargingStartedThreePhases:
            chargingState = "Not charging"
        elif self.chargingStartedOnePhase:
            chargingState = "Charging with 1 phase and {} A".format(self.currentAmpere)
        elif self.chargingStartedThreePhases:
            chargingState = "Charging with 3 phases and {} A".format(self.currentAmpere)
        self.log("Surplus: {:.0f} W; {}; Charged energy: {:.1f} kWh".format(
                 self.surplus, chargingState, self.chargedEnergy / 1000.0), 1)

        # ------------------------------ Examine the current surplus ------------------------------

        if self.surplus >= self.config["threePhasesMinP"]:
            # Three-phases minimum reached

            if not self.minimumReachedOnePhase:
                # Start silently, the message for three phases implies this
                self.minimumReachedOnePhase.start()

            if not self.minimumReachedThreePhases:
                self.log("Reached the three-phases charging minimum")
                self.minimumReachedThreePhases.start()

            if self.minimumUndercutOnePhase:
                self.minimumUndercutOnePhase.stop()

            if self.minimumUndercutThreePhases:
                self.minimumUndercutThreePhases.stop()

        elif self.surplus >= self.config["onePhaseMinP"]:
            # One-phase minimum reached (but not the three-phase minimum)

            if not self.minimumReachedOnePhase:
                self.log("Reached the one-phase charging minimum")
                self.minimumReachedOnePhase.start()

            if self.minimumReachedThreePhases:
                self.log("Undercut the three-phases charging minimum")
                self.minimumReachedThreePhases.stop()

            if self.minimumUndercutOnePhase:
                self.minimumUndercutOnePhase.stop()

            if not self.minimumUndercutThreePhases:
                self.minimumUndercutThreePhases.start()

        else:
            # Not enough for charging

            if self.minimumReachedOnePhase:
                self.log("Undercut the one-phase charging minimum")
                self.minimumReachedOnePhase.stop()

            if self.minimumReachedThreePhases:
                self.log("Undercut the three-phases charging minimum")
                self.minimumReachedThreePhases.stop()

            if not self.minimumUndercutOnePhase:
                self.minimumUndercutOnePhase.start()

            if not self.minimumUndercutThreePhases:
                self.minimumUndercutThreePhases.start()

        # ------------------------------- Check for a charging pause -------------------------------

        # If the last charging stop happened not long enough ago, stop here
        if self.chargingStopped and self.chargingStopped() < self.config["chargingPauseHoldTime"]:
            if self.minimumReachedOnePhase or self.minimumReachedThreePhases:
                self.log("Waiting for the minimum charging pause to pass ({} s to go)".format(
                         self.config["chargingPauseHoldTime"] - self.chargingStopped()), 1)
            return

        # --------------------- Inform about an ongoing charging start or stop ---------------------

        if not self.chargingStartedOnePhase \
           and self.minimumReachedOnePhase and not self.minimumReachedThreePhases \
           and not (self.chargingStartedThreePhases and self.minimumUndercutThreePhases):

            secs = self.config["onePhaseMinChargeTime"] - self.minimumReachedOnePhase()
            if secs > 0:
                self.log("Will start one-phase charging in {} s".format(secs), 1)
            else:
                self.log("Will now start one-phase charging", 1)

        if not self.chargingStartedThreePhases and self.minimumReachedThreePhases:
            secs = self.config["threePhasesMinChargeTime"] - self.minimumReachedThreePhases()
            if secs > 0:
                self.log("Will start three-phases charging in {} s".format(secs), 1)
            else:
                self.log("Will now start three-phases charging", 1)

        if self.chargingStartedOnePhase and self.minimumUndercutOnePhase:
            secs = self.config["onePhaseMinChargeTime"] - self.minimumUndercutOnePhase()
            if secs > 0:
                self.log("Will stop charging in {} s".format(secs), 1)
            else:
                self.log("Will now stop charging", 1)

        if self.chargingStartedThreePhases and self.minimumUndercutThreePhases:
            secs = self.config["threePhasesMinChargeTime"] - self.minimumUndercutThreePhases()

            if not self.minimumReachedOnePhase:
                if secs > 0:
                    self.log("Will stop charging in {} s".format(secs), 1)
                else:
                    self.log("Will now stop charging", 1)

            else:
                if secs > 0:
                    self.log("Will switch to one-phase charging in {} s".format(secs), 1)
                else:
                    self.log("Will now switch to one-phase charging", 1)

        # ---------------------------------- Three-Phases section ----------------------------------

        if not self.chargingStartedThreePhases:
            # At the moment, we don't charge with three phases

            if self.minimumReachedThreePhases() >= self.config["threePhasesMinChargeTime"]:
                # We can start three-phases charging
                self.startChargingThreePhases()
                return

            # We only return after having started three-phase charging
            # so that the one-phase section below will also be processed if not

        else:
            # At the moment, we do charge with three phases

            if self.minimumUndercutThreePhases() >= self.config["threePhasesMinChargeTime"]:
                # We can't charge with three phases anymore
                if self.minimumReachedOnePhase:
                    # We can continue charging with one phase
                    self.startChargingOnePhase()
                else:
                    # We can't charge anymore at all
                    self.stopCharging()
                return

            self.updateAmpere()
            return

        # ----------------------------------- One-Phase section -----------------------------------

        if not self.chargingStartedOnePhase:
            # At the moment, we don't charge with one phase

            if self.minimumReachedOnePhase() >= self.config["onePhaseMinChargeTime"]:
                # We can start one-phase charging
                self.startChargingOnePhase()
                return

        else:
            # At the moment, we do charge with one phase

            if self.minimumUndercutOnePhase() >= self.config["onePhaseMinChargeTime"]:
                # We can't charge anymore
                self.stopCharging()
                return

            self.updateAmpere()
            return
